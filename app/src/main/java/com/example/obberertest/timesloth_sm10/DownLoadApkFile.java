package com.example.obberertest.timesloth_sm10;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static android.content.Context.DOWNLOAD_SERVICE;

public class DownLoadApkFile extends BroadcastReceiver {

    public static boolean downLoad = false;
    private String fileVersion;
    private String urlDownload;
    private String fileName;
    private String destination;

    private long downloadId;
    private Uri uri;
    private String finalDestination;
    private DownloadManager manager;
    private ConnectActivity Connect_activity;
    private boolean isCreate = false;
    private String fileDownload;

    public DownLoadApkFile() {

    }

    public DownLoadApkFile(String name, String version, String s, ConnectActivity Connect_activity) {
        Log.d("downloadApkFile", s);
        this.Connect_activity = Connect_activity;
        this.isCreate = true;
        this.urlDownload = s;
        this.fileName = name;
        this.fileVersion = version;
        Log.d("version main", SiteData.Version_Build);
        if (!SiteData.Version_Build.equals(fileVersion)) {
            new FeedAsynTask().execute();
        }
        //get destination to update file and set Uri
        //TODO: First I wanted to store my update .apk file on internal storage for my app but apparently android does not allow you to open and install
        //application with existing package from there. So for me, alternative solution is Download directory in external storage. If there is better
        //solution, please inform us in comment

    }

    private void downloadFile(String fileDownload) {
        uri = Uri.parse("file://" + fileDownload);
        //Delete update file if exists
        File file = new File(fileDownload);
        if (file.exists()) {
            Log.d("downloadFile", "haveFile");
            file.delete();
            return;
        } else {
            Log.d("downloadFile", "no haveFile");
        }
        //getVersionInfo(file);
        //get url of app on server
        String url = urlDownload + "/" + fileName + ".apk";
        //url = "http://room.jaspal.co.th/apk/timesloth_horizontal.apk";

        //set downloadManager
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription("version " + fileVersion);
        request.setTitle(fileName);

        //set destination
        request.setDestinationUri(uri);

        // get download service and enqueue file
        manager = (DownloadManager) Connect_activity.getSystemService(DOWNLOAD_SERVICE);
        assert manager != null;
        downloadId = manager.enqueue(request);

        //set BroadcastReceiver to install app when .apk is downloaded
        finalDestination = destination;

        Log.d("apk", "registerReceiver");
        //register receiver for when .apk download is compete
        Connect_activity.registerReceiver(this, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

    }

    class FeedAsynTask extends AsyncTask<String, Void, String> {

        String feed = "FeedAsynTask";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Log.d(TAG, TAG_MODIFIED.tagFeed(feed) + " - " + TAG_MODIFIED.tagMethod("protected", "void", "onPreExecute"));
            //SiteData.writeFile(Connect_activity, TAG + " | " + TAG_MODIFIED.tagFeed(feed) + " - " + TAG_MODIFIED.tagMethod("protected", "void", "onPreExecute"));
        }

        @Override
        protected String doInBackground(String... strings) {
            //Log.d(TAG, TAG_MODIFIED.tagFeed(feed) + " - " + TAG_MODIFIED.tagMethod("protected", "String", "doInBackground"));
            //SiteData.writeFile(Connect_activity, TAG + " | " + TAG_MODIFIED.tagFeed(feed) + " - " + TAG_MODIFIED.tagMethod("protected", "String", "doInBackground"));
            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(5000, TimeUnit.MILLISECONDS);
            String url = "http://" + Connect_activity.Site_data.Server_address + Connect_activity.Site_data.Path + "api/services_api.php?key=apk_list";
            Log.d("OkHttp url apk file", url);
            //SiteData.writeFile(Connect_activity, TAG + " | " + "OkHttp url site config" + url);
            com.squareup.okhttp.Request GetSiteConfigData = new com.squareup.okhttp.Request.Builder().url(url).get().build();

            com.squareup.okhttp.Response getsiteconfigdata = null;
            String result_getsiteconfigdata = null;
            try {
                getsiteconfigdata = client.newCall(GetSiteConfigData).execute();
                result_getsiteconfigdata = getsiteconfigdata.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                //SiteData.writeFile(Connect_activity, TAG + " | FeedAsynTask doInBackground " + e.getMessage());
            }
            return result_getsiteconfigdata;
        }

        @SuppressLint("ResourceType")
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Log.d(TAG, TAG_MODIFIED.tagFeed(feed) + " - " + TAG_MODIFIED.tagMethod("protected", "void", "onPostExecute"));
            //SiteData.writeFile(Connect_activity, TAG + " | " + TAG_MODIFIED.tagFeed(feed) + " - " + TAG_MODIFIED.tagMethod("protected", "void", "onPostExecute"));
            if (s != null) {
                Log.d("OkHttp url apk file", s);
                destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    jsonObject = jsonObject.getJSONObject("data");
                    JSONArray apkArray = jsonObject.getJSONArray("apk");
                    for (int i = 0; i < apkArray.length(); i++) {
                        String url = apkArray.get(i).toString();
                        String _fileName = url.substring(url.lastIndexOf('/') + 1);
                        String filenameArray[] = _fileName.split("\\.");
                        //String extension = filenameArray[filenameArray.length-1];
                        Log.d("url", url);
                        Log.d("fileName filenameArray", fileName + " - " + filenameArray[0]);

                        if (fileName.equals(filenameArray[0])) {
                            fileDownload = destination + fileName;
                            File file = new File(fileDownload);
                            if (file.exists()) {
                                file.delete();
                            }
                            downloadFile(fileDownload);
                            return;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                Log.d("OkHttp url apk file", "null");
            }
        }
    }

    private String getVersionNameMain() {
        String versionName = "";
        try {
            PackageInfo packageInfo = Connect_activity.getPackageManager().getPackageInfo(Connect_activity.getPackageName(), 0);
            versionName = packageInfo.versionName;
            return versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
        //textViewVersionInfo.setText(String.format("Version name = %s \nVersion code = %d", versionName, versionCode));
    }

    private String getVersionNameFile() {
        try {
            ApplicationInfo ai = Connect_activity.getPackageManager().getApplicationInfo(Connect_activity.getPackageName(), 0);
            ZipFile zf = new ZipFile(ai.sourceDir);
            ZipEntry ze = zf.getEntry("file://" + destination + fileName + ".apk");
            long time = 0;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                time = ze.getTime();
            }
            String s = SimpleDateFormat.getInstance().format(new java.util.Date(time));
            return s;
        } catch (Exception e) {
        }
        return "";
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!isCreate) return;
        Log.d("apk", "onreceive");
        Log.d("BroadcastReceiver", "App Installing...Please Wait");

        assert finalDestination != null;
        assert Connect_activity != null;
        Connect_activity.unregisterReceiver(this);


        Log.d("version file", getVersionNameFile());

        Intent install = new Intent(Intent.ACTION_VIEW);
        install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        install.setDataAndType(uri,
                "application/vnd.android.package-archive");
        Connect_activity.startActivityForResult(install, 1);


        downLoad = true;
        //Connect_activity.finish();
    }
}
