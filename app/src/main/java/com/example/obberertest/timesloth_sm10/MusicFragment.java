package com.example.obberertest.timesloth_sm10;


import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class MusicFragment extends Fragment {


    public static boolean autoPauseMusic;
    public static boolean autoPlayMusic;
    public static boolean playMusic;
    private Random random;
    private MainActivity mMainActivity;
    private View View_main;
    private List<String> listMusic = new ArrayList<>();
    private Thread musicThread;
    private MediaPlayer mPlayer;
    private boolean isPrepareMusic = false;

    public MusicFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public MusicFragment(MainActivity context, boolean playMusic, boolean autoPlayMusic, boolean autoPauseMusic) {
        this.mMainActivity = context;
        MusicFragment.playMusic = playMusic;
        MusicFragment.autoPlayMusic = autoPlayMusic;
        MusicFragment.autoPauseMusic = autoPauseMusic;
        random = new Random();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View_main = inflater.inflate(R.layout.fragment_music, container, false);
        new FeedAsynTaskMusic().execute();
        return View_main;
    }

    private void runThread() {
        musicThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        mMainActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (mMainActivity.Main_Room.RoomStatus > 0 && autoPauseMusic) {
                                    if (mPlayer != null && mPlayer.isPlaying()) {
                                        mPlayer.stop();
                                        mPlayer.reset();
                                    }
                                } else if (mPlayer != null && !isPrepareMusic) {
                                    if (playMusic && !mPlayer.isPlaying()) {
                                        runPlayMusic();
                                    } else if (!playMusic && mPlayer.isPlaying()) {
                                        mPlayer.stop();
                                        mPlayer.reset();
                                    }
                                } else if (playMusic && !isPrepareMusic) {
                                    runPlayMusic();
                                }
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        musicThread.start();
    }

    private void runPlayMusic() {
        int index = random.nextInt(listMusic.size());
        Uri uri = Uri.parse(listMusic.get(index));

        // Initialize the media player
        isPrepareMusic = true;
        mPlayer = MediaPlayer.create(mMainActivity, uri);
        mPlayer.setOnPreparedListener(mp -> {
            mPlayer.start();
            isPrepareMusic = false;
        });
        mPlayer.setOnCompletionListener(mp -> {
            mPlayer.reset();
        });
        mPlayer.setOnErrorListener((mp, what, extra) -> {
            mPlayer.reset();
            isPrepareMusic = false;
            return false;
        });
    }

    class FeedAsynTaskMusic extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Log.d(TAG, TAG_MODIFIED.tagFeed(feed) + " - " + TAG_MODIFIED.tagMethod("protected", "void", "onPreExecute"));
            //SiteData.writeFile(Main_activity, TAG + " | " + TAG_MODIFIED.tagFeed(feed) + " - " + TAG_MODIFIED.tagMethod("protected", "void", "onPreExecute"));
        }

        @Override
        protected String doInBackground(String... strings) {
            //Log.d(TAG, TAG_MODIFIED.tagFeed(feed) + " - " + TAG_MODIFIED.tagMethod("protected", "String", "doInBackground"));
            //SiteData.writeFile(Main_activity, TAG + " | " + TAG_MODIFIED.tagFeed(feed) + " - " + TAG_MODIFIED.tagMethod("protected", "String", "doInBackground"));
            OkHttpClient client = new OkHttpClient();
            String url = "http://" + mMainActivity.Site_data.Server_address + mMainActivity.Site_data.Path + "api/services_api.php?key=music_list";
            //String url = "http://" + Main_activity.Site_data.Server_address + Main_activity.Site_data.Path + "api/services.php?action=get&key=reservation_by_resourceid_date&resourceid=" + String.valueOf(Main_activity.Main_Room.Id) + "&date=" + "2017-12-22";
            Log.d("OkHttp url music", url);
            //SiteData.writeFile(Main_activity, TAG + " | " + "OkHttp url getRoomInfoRoomID " + url);
            com.squareup.okhttp.Request request = new com.squareup.okhttp.Request.Builder().url(url).get().build();
            com.squareup.okhttp.Response response = null;
            String result = null;
            try {
                response = client.newCall(request).execute();
                result = response.body().string();
                client.cancel("TAG");
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //SiteData.writeFile(Main_activity, TAG + " | " + TAG_MODIFIED.tagFeed(feed) + " - " + TAG_MODIFIED.tagMethod("protected", "void", "onPostExecute"));
            //Log.d("OkHttp get getRoomInfoRoomID",s);
            if(!s.equals("")){
                try {
                    JSONObject json = new JSONObject(s);
                    JSONObject data = json.getJSONObject("data");
                    JSONArray musicList = data.getJSONArray("music");
                    for (int i=0;i<musicList.length();i++) {
                        listMusic.add(musicList.get(i).toString());
                    }
                    if (musicList.length() > 0) {
                        runThread();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
