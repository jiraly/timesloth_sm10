package com.example.obberertest.timesloth_sm10;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class ModuleConnectionFailFragment extends Fragment {

    private static View View_main;
    private static View Popup;
    private boolean fail = false;

    public ModuleConnectionFailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View_main = inflater.inflate(R.layout.fragment_module_connection_fail, container, false);
        Popup = View_main.findViewById(R.id.popup_box);
        hideLoader();
        return View_main;
    }

    static void hideLoader(){
        //Log.d(TAG, TAG_MODIFIED.tagMethod("", "void", "hideLoader"));
        //SiteData.writeFile(Main_activity, TAG + " | " + TAG_MODIFIED.tagMethod("", "void", "hideLoader"));
        Popup.setVisibility(View.INVISIBLE);
        setFail(false);
    }

    static void showLoader(){
        //Log.d(TAG, TAG_MODIFIED.tagMethod("", "void", "showLoader"));
        //SiteData.writeFile(Main_activity, TAG + " | " + TAG_MODIFIED.tagMethod("", "void", "showLoader"));
        Popup.setVisibility(View.VISIBLE);
        setFail(true);
    }

    public static View getView_main() {
        return View_main;
    }

    public static void setView_main(View view_main) {
        View_main = view_main;
    }

    public static View getPopup() {
        return Popup;
    }

    public static void setPopup(View popup) {
        Popup = popup;
    }

    public boolean isFail() {
        return fail;
    }

    public static void setFail(boolean fail) {
        fail = fail;
    }
}
