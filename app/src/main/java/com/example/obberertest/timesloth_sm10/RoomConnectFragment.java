package com.example.obberertest.timesloth_sm10;


import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 */
public class RoomConnectFragment extends Fragment {

    static String TAG = "RoomConnectFragment";

    private ConnectActivity Connect_activity;
    private RoomConnectFragment Context;
    private View View_main;
    public boolean Finish;
    boolean isConnected = false;

    public RoomConnectFragment() {
        // Required empty public constructor
        //Log.d(TAG, TAG_MODIFIED.tagMethod("public", "", "RoomConnectFragment"));
        //SiteData.writeFile(Connect_activity, TAG + " | " + TAG_MODIFIED.tagMethod("public", "", "RoomConnectFragment"));
    }

    @SuppressLint("ValidFragment")
    public RoomConnectFragment(ConnectActivity _connectactivity) {
        // Required empty public constructor
        //Log.d(TAG, TAG_MODIFIED.tagMethod("public", "", "RoomConnectFragment") + " - " + TAG_MODIFIED.tagArgument("ConnectActivity", "_connectactivity", String.valueOf(_connectactivity)));
        Context = this;
        Connect_activity = _connectactivity;
        //SiteData.writeFile(Connect_activity, TAG + " | " + TAG_MODIFIED.tagMethod("public", "", "RoomConnectFragment") + " - " + TAG_MODIFIED.tagArgument("ConnectActivity", "_connectactivity", String.valueOf(_connectactivity)));
        this.Finish = false;
    }

    public void updateRoom(ConnectActivity _connectactivity){
        //Log.d(TAG, TAG_MODIFIED.tagMethod("public", "void", "updateRoom") + " - " + TAG_MODIFIED.tagArgument("ConnectActivity", "_connectactivity", String.valueOf(_connectactivity)));
        //SiteData.writeFile(Connect_activity, TAG + " | " + TAG_MODIFIED.tagMethod("public", "void", "updateRoom") + " - " + TAG_MODIFIED.tagArgument("ConnectActivity", "_connectactivity", String.valueOf(_connectactivity)));
        Connect_activity = _connectactivity;
        this.Finish = false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //Log.d(TAG, TAG_MODIFIED.tagMethod("public", "View", "onCreateView"));
        //SiteData.writeFile(Connect_activity, TAG + " | " + TAG_MODIFIED.tagMethod("public", "View", "onCreateView"));
        View_main = inflater.inflate(R.layout.fragment_room_connect, container, false);
        bindButton();
        bindSetting();
        return View_main;
    }

    private void bindButton() {
        //Log.d(TAG, TAG_MODIFIED.tagMethod("private", "void", "bindButton"));
        //SiteData.writeFile(Connect_activity, TAG + " | " + TAG_MODIFIED.tagMethod("private", "void", "bindButton"));
        Button btn_save = View_main.findViewById(R.id.btn_connect_setting_save);
        Button btn_ipconfig = View_main.findViewById(R.id.btn_connect_setting_ipconfig);
        Button btn_cancel = View_main.findViewById(R.id.btn_connect_setting_cancel);
        Button btn_setting = View_main.findViewById(R.id.btn_connect_setting_setting);

        btn_setting.setOnClickListener(v -> {
            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            Connect_activity.finish();
        });

        btn_cancel.setOnClickListener(view -> {
            Connect_activity.Network_diag_fragment.startConnectNetwork();
            try {
                Connect_activity.getFragmentManager().beginTransaction().remove(Connect_activity.Room_connect_fragment).commit();
            } catch (IllegalStateException e) {
                SiteData.writeFile(Connect_activity, TAG + " | btn_connect_setting_save " + e.getMessage());

            }
        });

        btn_save.setOnClickListener(view -> {
            SiteData.playSound(Connect_activity, "ok");
            //Log.d(TAG, TAG_MODIFIED.tagOnClick("btn_connect_setting_save", "Button"));
            //SiteData.writeFile(Connect_activity, TAG + " | " + TAG_MODIFIED.tagOnClick("btn_connect_setting_save", "Button"));
            //NumberPicker room = View_main.findViewById(R.id.numberpicker_connect_setting_select_room);
            EditText room_number = View_main.findViewById(R.id.edittext_number_room);
            EditText server_address = View_main.findViewById(R.id.edittext_connect_setting_serveraddress);
//                if(Connect_activity.All_Room.size() > 0) {
//                    Connect_activity.Site_data.Room_id = Connect_activity.All_Room.get(room.getValue()).Id;
//                }
            Log.d(String.valueOf(Connect_activity.Site_data.Room_id), room_number.getText().toString());
            Connect_activity.Site_data.Room_id = Integer.parseInt(room_number.getText().toString());
            Connect_activity.Site_data.Server_address = server_address.getText().toString();
            //Button save = Connect_main.findViewById(R.id.Connect_btn_setting);
            //save.performClick();
            Connect_activity.Room_connect_fragment.updateRoom(Connect_activity);
            Connect_activity.Site_connect_fragment.updateSiteData(Connect_activity, Connect_activity.Site_data.Server_address, Connect_activity.Site_data.Room_id, Connect_activity.Site_data.Path);
            Connect_activity.Network_diag_fragment.startConnectNetwork();
            try {
                Connect_activity.getFragmentManager().beginTransaction().remove(Connect_activity.Room_connect_fragment).commit();
            } catch (IllegalStateException e) {
                SiteData.writeFile(Connect_activity, TAG + " | btn_connect_setting_save " + e.getMessage());
                Connect_activity.getFragmentManager().beginTransaction().remove(Connect_activity.Room_connect_fragment).commitAllowingStateLoss();
            }
        });

        btn_ipconfig.setOnClickListener(view -> {
            SiteData.playSound(Connect_activity, "ok");
            //Log.d(TAG, TAG_MODIFIED.tagOnClick("btn_connect_setting_ipconfig", "Button"));
            //SiteData.writeFile(Connect_activity, TAG + " | " + TAG_MODIFIED.tagOnClick("btn_connect_setting_ipconfig", "Button"));
            Intent intent = new Intent(Connect_activity, NetworkSettingActivity.class);
            startActivity(intent);
            //startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            Connect_activity.finish();
            //startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
        });
    }

    class FeedAsynTask extends AsyncTask<String, Void, String> {

        String feed = "FeedAsynTask";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            Connect_activity.All_Room.removeAll(Connect_activity.All_Room);
            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(5000, TimeUnit.MILLISECONDS);
            String url = "http://" + Connect_activity.Site_data.Server_address + Connect_activity.Site_data.Path + "api/services_api.php?key=resource_by_resourceid&resource_id=" + String.valueOf(Connect_activity.Site_data.Room_id);
            //String url = "http://" + Connect_activity.Site_data.Server_address + Connect_activity.Site_data.Path + "api/services_api.php?key=resource_by_resourceid&resource_id=" + String.valueOf(Connect_activity.Site_data.Room_id);
            Log.d("OkHttp url resourceId", url);
            //SiteData.writeFile(Connect_activity, TAG + " | " + "OkHttp url all room" + url);
            com.squareup.okhttp.Request request = new com.squareup.okhttp.Request.Builder().url(url).get().build();
            com.squareup.okhttp.Response response = null;
            String result = null;
            try {
                response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (IOException e) {
                e.printStackTrace();
                SiteData.writeFile(Connect_activity, TAG + " | FeedAsynTask doInBackground " + e.getMessage());
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!Objects.equals(s, "")) {
                try {
                    JSONObject all_room_resource = new JSONObject(s);
                    all_room_resource = all_room_resource.getJSONObject("data");
                    Log.d("OkHttp get all room", all_room_resource.toString());
                    Connect_activity.All_Room.add(new Room(all_room_resource));
                    new AnnounceFeedAsynTask().execute();
                } catch (JSONException e) {
                    e.printStackTrace();
                    SiteData.writeFile(Connect_activity, TAG + " | FeedAsynTask onPostExecute " + e.getMessage());
                }
            }
        }
    }

    class AnnounceFeedAsynTask extends AsyncTask<String, Void, String> {

        String feed = "FeedAsynTask";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(5000, TimeUnit.MILLISECONDS);
            String url = "http://" + Connect_activity.Site_data.Server_address + Connect_activity.Site_data.Path + "api/services_api.php?key=reservation_announce&resource_id=" + String.valueOf(Connect_activity.Site_data.Room_id);
            //String url = "http://" + Connect_activity.Site_data.Server_address + Connect_activity.Site_data.Path + "api/services_api.php?key=resource_by_resourceid&resource_id=" + String.valueOf(Connect_activity.Site_data.Room_id);
            Log.d("OkHttp url announce", url);
            com.squareup.okhttp.Request request = new com.squareup.okhttp.Request.Builder().url(url).get().build();
            com.squareup.okhttp.Response response = null;
            String result = null;
            try {
                response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (IOException e) {
                e.printStackTrace();
                SiteData.writeFile(Connect_activity, TAG + " | FeedAsynTask doInBackground " + e.getMessage());
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!Objects.equals(s, "")) {
                try {
                    JSONObject all_room_resource = new JSONObject(s);
                    all_room_resource = all_room_resource.getJSONObject("data");
                    if (all_room_resource.getBoolean("status")) {
                        Finish = true;
                        isConnected = true;
                        Connect_activity.Network_diag_fragment.isConnectSite = false;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    SiteData.writeFile(Connect_activity, TAG + " | FeedAsynTask onPostExecute " + e.getMessage());
                }
            }
        }
    }

    private void bindSetting(){
        //Log.d(TAG, TAG_MODIFIED.tagMethod("private", "void", "bindSetting"));
        //SiteData.writeFile(Connect_activity, TAG + " | " + TAG_MODIFIED.tagMethod("private", "void", "bindSetting"));
        //int maxValue = Connect_activity.All_Room.size();
        //NumberPicker select_room = View_main.findViewById(R.id.numberpicker_connect_setting_select_room);
        EditText room_number = View_main.findViewById(R.id.edittext_number_room);
        EditText server_address = View_main.findViewById(R.id.edittext_connect_setting_serveraddress);

        server_address.setText(Connect_activity.Site_data.Server_address);
        room_number.setText(String.valueOf(Connect_activity.Site_data.Room_id));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //getFragmentManager().beginTransaction().remove(RoomConnectFragment.this).commit();
    }
}
